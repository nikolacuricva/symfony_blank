FROM php:8.1-apache

RUN rm -rf /var/lib/mysql/*
RUN apt-get update \
 && apt-get install -y git zlib1g-dev libicu-dev g++ nano libpng-dev libonig-dev libzip-dev libpq-dev libmagickwand-dev librabbitmq-dev libreadline-dev pkg-config apt-transport-https \
 && docker-php-ext-install mbstring pdo pdo_mysql zip mysqli gd \
 && docker-php-ext-install pgsql pdo_pgsql bcmath\
 && docker-php-ext-configure intl \
 && docker-php-ext-install intl \
 && pecl install xdebug redis amqp mongodb \
 && docker-php-ext-enable xdebug redis amqp mongodb \
     && echo "xdebug.mode=develop,debug" >> /usr/local/etc/php/conf.d/xdebug.ini \
     && echo "xdebug.start_with_request=yes" >> /usr/local/etc/php/conf.d/xdebug.ini \
     && echo "xdebug.discover_client_host=0" >> /usr/local/etc/php/conf.d/xdebug.ini \
     && echo "xdebug.client_host=host.docker.internal" >> /usr/local/etc/php/conf.d/xdebug.ini \
     && echo "xdebug.client_port=9003" >> /usr/local/etc/php/conf.d/xdebug.ini \
 && a2enmod rewrite headers \
 && sed -i 's!/var/www/html!/var/www/public!g' /etc/apache2/sites-available/000-default.conf \
 && curl -sS https://getcomposer.org/installer \
  | php -- --install-dir=/usr/local/bin --filename=composer

RUN curl -1sLf 'https://dl.cloudsmith.io/public/symfony/stable/setup.deb.sh' | bash
RUN apt install symfony-cli

WORKDIR /var/www

#EXPOSE 80
#EXPOSE 443
