<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231028134906 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        if (!$schema->hasTable('user')) {
            $sql = <<<SQL
            CREATE TABLE users (
                id BIGSERIAL PRIMARY KEY,
                first_name VARCHAR(50) NOT NULL,
                last_name VARCHAR(50) NOT NULL,
                email VARCHAR(75) NOT NULL
            );
SQL;
            $this->addSql($sql);
        }
    }

    public function down(Schema $schema): void
    {
        if ($schema->hasTable('users')){
            $sql = <<<SQL
            DROP TABLE users;
SQL;
            $this->addSql($sql);
        }
    }
}
