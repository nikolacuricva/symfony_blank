<?php

namespace App\dto;

class UserDto
{
    public function __construct(
     public readonly string $firstName,
     public readonly string $lastName,
     public readonly string $email
    ) {}
}