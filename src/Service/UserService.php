<?php

namespace App\Service;

use App\dto\UserDto;
use App\Entity\User;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UserService
{
    private EntityManagerInterface $entityManager;
    private DocumentManager $documentManager;
    private ValidatorInterface $validator;

    public function __construct(
        EntityManagerInterface $entityManager,
        DocumentManager $documentManager,
        ValidatorInterface $validator
    )
    {
        $this->entityManager = $entityManager;
        $this->documentManager = $documentManager;
        $this->validator = $validator;
    }

    public function getDatabase() :string
    {

//        dd($this->documentManager->getClient()->selectDatabase('user')->command());

        return $this->entityManager->getConnection()->getDatabase() . " from service";
    }

    public function addUser(UserDto $userDto) :User|ConstraintViolationListInterface
    {
        $user = new User($userDto);

        $errors = $this->validator->validate($user);

        if (count($errors) > 0){
            return $errors;
        }

        $this->entityManager->persist($user);
        $this->entityManager->flush();
        return $user;
    }

    public function updateUser(User $user, UserDto $userDto) :User
    {
        $user->selfUpdate($userDto);
        $this->entityManager->flush();
        return $user;
    }

}