<?php

namespace App\Controller;

use App\dto\UserDto;
use App\Entity\User;
use App\Service\UserService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class HomeController extends AbstractController
{
    private EntityManagerInterface $entityManager;

    private UserService $userService;

    public function __construct(EntityManagerInterface $entityManager, UserService $userService)
    {
        $this->entityManager = $entityManager;
        $this->userService = $userService;
    }

    #[Route('/', name: 'home')]
    public function index(): Response
    {
        return $this->render('home/index.html.twig', [
            'twig_var' => 'Variable content',
        ]);
    }

    #[Route('/add-user', name: 'add-user', methods: ['POST'])]
    public function addUser(Request $request): Response
    {
        dd($request->getLocale());
        $userDto = new UserDto(
            $request->request->get('firstName', null),
            $request->request->get('lastName', null),
            $request->request->get('email', null)
        );
        $result = $this->userService->addUser($userDto);
        
        if ($result instanceof ConstraintViolationListInterface){
            foreach ($result as $item) {
                dd($item->getMessageTemplate());
            }
        }
        
        return $this->render('home/index.html.twig', [
            'user' => $result,
        ]);
    }

    #[Route('/edit-user', name: 'edit-user', methods: ['POST'])]
    public function editUser(Request $request) :Response
    {
        $id = $request->request->get('userId', 0);
        $user = $this->entityManager->find(User::class, $id);
        !empty($user)?:dd("No such user");
        $userDto = new UserDto(
            $request->request->get('firstName', null),
            $request->request->get('lastName', null),
            $request->request->get('email', null)
        );

        $user = $this->userService->updateUser($user, $userDto);
        return $this->render('home/index.html.twig', [
            'user' => $user,
        ]);
    }

}
