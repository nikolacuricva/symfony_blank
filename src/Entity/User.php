<?php

namespace App\Entity;

use App\dto\UserDto;
use App\Repository\UserRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints\NotBlank;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\Table(name: 'users')]
#[UniqueEntity(
    fields: ['email'],
    message: "{{ label }} {{ value }} je vec u upotrebi.",
    errorPath: 'email',
)]
class User
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(name: "first_name", length: 50)]
    private ?string $firstName = null;

    #[ORM\Column(name:"last_name", length: 50)]
    private ?string $lastName = null;

    #[ORM\Column(name:"email", length: 50, unique: true)]
    #[NotBlank(message: 'Polje email ne sme biti prazno')]
    private string $email;

    /**
     * @param UserDto $userDto
     */
    public function __construct(UserDto $userDto)
    {
        $this->firstName = $userDto->firstName;
        $this->lastName = $userDto->lastName;
        $this->email = $userDto->email;
    }

    public function selfUpdate(UserDto $userDto) :self
    {
        $this->setFirstName($userDto->firstName);
        $this->setLastName($userDto->lastName);
        $this->setEmail($userDto->email);
        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): static
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): void
    {
        $this->lastName = $lastName;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }
}
